@extends('layouts.master')

@section('title')
Edit Penulis by Id = {{$penulis->id}} Form
@endsection

@section('content')
<form action="/penulis/{{$penulis->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
    <label>Nama</label>
    <input type="text" value="{{$penulis->nama}}" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Masukan Nama Penulis">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Usia</label>
    <input type="text" value="{{$penulis->usia}}" name="usia" class="@error('usia') is-invalid @enderror form-control" placeholder="Masukan Usia Penulis">
    </div>
    @error('usia')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Biodata</label>
        <textarea type="text" name="bio" class="@error('bio') is-invalid @enderror form-control" cols="30" rows="10" placeholder="Masukan Biodata Penulis">{{ $penulis->bio }}</textarea>
      </div>
  
      @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<form action="/penulis/{{$penulis->id}}" method="POST">
    @csrf
    @method('delete')
        <input type="submit" value="Delete" class="btn btn-danger">
</form>

@endsection
