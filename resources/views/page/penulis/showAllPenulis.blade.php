@extends('layouts.master')

@section('title')
List Penulis
@endsection

@section('content')
<a href="/penulis/create" class="btn btn-primary btn-sm my-3">Tambah</a>
<br><br>
<h1>List Penulis</h1>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Usia</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($penulis as $key => $penulis_item)
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$penulis_item->nama}}</td>
            <td>{{$penulis_item->usia}}</td>
            <td>
                <form action="/penulis/{{$penulis_item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/penulis/{{$penulis_item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/penulis/{{$penulis_item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-sm btn-danger">
                </form>
            </td>
          </tr>
        @empty
            <tr>
                <td>Belum ada data penulis</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection