@extends('layouts.master')

@section('title')
Isi Data Penulis
@endsection

@section('content')
<form action="/penulis" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Masukan Nama Penulis">
    </div>

    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Usia</label>
      <input type="text" name="usia" class="@error('usia') is-invalid @enderror form-control" placeholder="Masukan Usia Penulis">
    </div>

    @error('usia')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Biodata</label>
      <textarea type="text" name="bio" class="@error('bio') is-invalid @enderror form-control" cols="30" rows="10" placeholder="Masukan Biodata Penulis"></textarea>
    </div>

    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection