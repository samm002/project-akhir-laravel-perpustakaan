@extends('layouts.master')

@section('title')
Show Penulis by Id = {{$penulis->id}}
@endsection

@section('content')

<div class="card mb-4 d-flex px-auto">
  <div class="card-body">
    <p class="card-text"><b>Nama : </b>{{$penulis->nama}}</p>
    <p class="card-text"><b>Usia : </b>{{$penulis->usia}}</p>
    <p class="card-text"><b>Biodata : </b>{{$penulis->bio}}</p>
  </div>
</div>
<a href="/penulis" class="btn btn-secondary btn-sm my-3">kembali</a>
@endsection