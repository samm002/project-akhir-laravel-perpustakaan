@extends('layouts.master')

@section('title')
Show All Transaksi
@endsection

@section('content')
<h1>List Transaksi</h1>
<a href="/transaksi/create" class="btn btn-primary btn-sm my-3">Tambah</a>
<table class="table">
    <thead class="thead-dark">
      <tr class="text-center allign-item-middle" style="justify-content: center;">
        <th scope="col">No</th>
        <th scope="col">Tanggal Peminjaman</th>
        <th scope="col">Status</th>
        <th scope="col">Nama Peminjam</th>
        <th scope="col">Email Peminjam</th>
        <th scope="col">Buku yang dipinjam</th>
        <th scope="col">Petugas yang melayani</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($transaksi as $key => $transaksi_item)
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$transaksi_item->tanggal_peminjaman}}</td>
            <td>{{$transaksi_item->status}}</td>
            <td>{{$transaksi_item->peminjam->nama}}</td>
            <td>{{$transaksi_item->peminjam->email}}</td>
            <td>{{$transaksi_item->buku->judul}}</td>
            <td>{{$transaksi_item->petugas->user->name}}</td>
            <td>
                <form action="/transaksi/{{$transaksi_item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/transaksi/{{$transaksi_item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/transaksi/{{$transaksi_item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-sm btn-danger">
                </form>
            </td>
          </tr>
        @empty
            <tr>
                <td>Belum ada data transaksi</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection