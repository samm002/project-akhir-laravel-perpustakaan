@extends('layouts.master')

@section('title')
Show Transaksi by Id = {{$transaksi->id}}
@endsection

@section('content')
<div class="card mb-4 d-flex px-auto">
  <div class="card-body">
    <h5 class="card-title">{{$transaksi->tanggal_peminjaman}}</h5> 
    <p class="card-text">{{$transaksi->status}}</p>
    @if ($transaksi->pengembalian_id !== null)
    <a href="/pengembalian/{{$transaksi->pengembalian_id}}">Lihat detail</a>
    @endif
    <p class="card-text"><b>Nama Peminjam: </b>{{$transaksi->peminjam->nama}}</p>
    <p class="card-text"><b>Email Peminjam : </b>{{$transaksi->peminjam->email}}</p>
    <p class="card-text"><b>Judul Buku yang Dipinjam : </b>{{$transaksi->buku->judul}}</p>
    <p class="card-text"><b>Petugas yang melayani : </b>{{$transaksi->petugas->user->name}}</p>
  </div>
  @auth
  <div class="card-footer d-flex">
    <a href="/transaksi/{{$transaksi->id}}/edit" class="btn btn-warning btn-sm mx-2">Edit</a>
    <button type="button" class="btn btn-danger btn-sm mx-2" data-toggle="modal" data-target="#deleteModal{{$transaksi->id}}">
      Delete
    </button>
  </div>
  @endauth
</div>
@endsection