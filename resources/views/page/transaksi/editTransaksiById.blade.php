@extends('layouts.master')

@section('title')
Edit Transaksi by Id = {{$transaksi->id}} Form
@endsection

@section('content')
<form action="/transaksi/{{$transaksi->id}}" method="POST">
    @csrf
    @method('put')
    
    <div class="form-group">
        <label>Tanggal Peminjaman</label>
        <input type="text" value="{{$transaksi->tanggal_peminjaman}}" name="tanggal_peminjaman" class="@error('tanggal_peminjaman') is-invalid @enderror form-control" placeholder="Masukan Tanggal Peminjaman">
    </div>
    @error('tanggal_peminjaman')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Status</label>
    <input type="text" value="{{$transaksi->status}}" name="status" class="@error('status') is-invalid @enderror form-control" placeholder="Masukan Status Transaksi">
    </div>
    @error('status')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Peminjam</label>
        <select class="form-control" name="peminjam_id">
            <option value="">Peminjam</option>
            @forelse ($peminjam as $item)
            <option value="{{$item->id}}">{{$item->nama}} ({{ $item->email }})</option>
            @empty
                <option value="">Tidak Ada peminjam</option>
            @endforelse
        </select>
    </div>
    @error('peminjam_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="buku-dropdown">Pilih Buku</label>
        <select class="form-control" id="buku-dropdown">
            <option value="">Pilih Buku</option>
            @forelse ($buku as $item)
                <option value="{{$item->id}}">{{$item->judul}}</option>
            @empty
                <option value="">Tidak Ada buku</option>
            @endforelse
        </select>
    </div>
    
    <div>
        <h4>Selected Books:</h4>
        <ul id="selected-books">
            <!-- Display selected books here -->
        </ul>
    </div>

    <div class="form-group">
        <label>Petugas</label>
        <select class="form-control" name="petugas_id">
          <option value="">Petugas saat meminjam</option>
          @forelse ($petugas as $item)
              <option value="{{$item->id}}">{{$item->nama}}</option>
          @empty
              <option value="">Tidak Ada petugas</option>
          @endforelse
      </select>
      </div>
      @error('petugas')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Pengembalian</label>
        <select class="form-control" name="pengembalian_id">
          <option value="">Keterangan Pengembalian</option>
          @forelse ($pengembalian as $item)
              <option value="{{$item->id}}">{{$item->tanggal_pengembalian}}</option>
              <option value="{{$item->id}}">{{$item->denda}}</option>
          @empty
              <option value="">Belum Dikembalikan</option>
          @endforelse
      </select>
      </div>
      @error('pengembalian')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<form action="/transaksi/{{$transaksi->id}}" method="POST">
    @csrf
    @method('delete')
        <input type="submit" value="Delete" class="btn btn-danger">
</form>

<script>
    // JavaScript to handle book selection and removal
    const selectedBooks = [];

    const bukuDropdown = document.getElementById('buku-dropdown');
    const selectedBooksList = document.getElementById('selected-books');
    const selectedBooksInput = document.getElementById('selected-books-input');
    const addBookButton = document.getElementById('add-book');

    bukuDropdown.addEventListener('change', () => {
        const selectedBookId = bukuDropdown.value;
        if (selectedBookId && !selectedBooks.includes(selectedBookId)) {
            selectedBooks.push(selectedBookId);
            const selectedBookText = bukuDropdown.options[bukuDropdown.selectedIndex].text;
            const listItem = document.createElement('li');
            listItem.textContent = selectedBookText;
            selectedBooksList.appendChild(listItem);
            
            // Remove the selected option from the dropdown
            const selectedOption = bukuDropdown.querySelector(`[value="${selectedBookId}"]`);
            if (selectedOption) {
                selectedOption.remove();
            }
        }
    });

    addBookButton.addEventListener('click', () => {
        bukuDropdown.value = ''; // Clear the selection
    });

    // Handle form submission
    const form = document.querySelector('form');
    form.addEventListener('submit', () => {
        selectedBooksInput.value = JSON.stringify(selectedBooks);
    });
</script> 

@endsection
