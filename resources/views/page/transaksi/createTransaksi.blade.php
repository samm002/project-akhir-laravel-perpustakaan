@extends('layouts.master')

@section('title')
Isi Data Transaksi
@endsection

@section('content')
<form action="/transaksi" method="POST">
    @csrf
    <div class="form-group">
        <label>Tanggal Peminjaman</label>
        <input type="date" name="tanggal_peminjaman" class="@error('tanggal_peminjaman') is-invalid @enderror form-control" placeholder="Masukan Tanggal Peminjaman">
    </div>
    @error('tanggal_peminjaman')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Status</label>
        <select class="form-control" name="status" id="statusDropdown">
            <option value="dipinjam" {{ old('status') == 'dipinjam' ? 'selected' : '' }}>Dipinjam</option>
            <option value="dikembalikan" {{ old('status') == 'dikembalikan' ? 'selected' : '' }}>Dikembalikan</option>
        </select>
    </div>
    <div class="form-group" id="pengembalianFields" style="display: none;">
        <div class="form-group">
            <label>Tanggal Pengembalian</label>
            <input type="date" name="tanggal_pengembalian" class="form-control">
        </div>
        <div class="form-group">
            <label>Denda</label>
            <input type="text" name="denda" class="form-control">
        </div>
    </div>
    @error('status')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Peminjam</label>
        <select class="form-control" name="peminjam_id">
            <option value="">Peminjam</option>
            @forelse ($peminjam as $item)
            <option value="{{$item->id}}">{{$item->nama}} ({{ $item->email }})</option>
            @empty
                <option value="">Tidak Ada peminjam</option>
            @endforelse
        </select>
    </div>
    @error('peminjam_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Buku</label>
      <select class="form-control" name="buku_id">
        <option value="">Buku yang dipinjam</option>
        @forelse ($buku as $item)
            <option value="{{$item->id}}">{{$item->judul}}</option>
        @empty
            <option value="">Tidak Ada Buku</option>
        @endforelse
    </select>
    </div>
    @error('buku')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Petugas</label>
      <select class="form-control" name="petugas_id">
        <option value="">Petugas saat meminjam</option>
        @forelse ($petugas as $item)
            <option value="{{$item->id}}">{{$item->user->name}}</option>
        @empty
            <option value="">Tidak Ada petugas</option>
        @endforelse
    </select>
    </div>
    @error('petugas')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form> 

<script>
    const statusDropdown = document.querySelector('#statusDropdown');
    const pengembalianFields = document.getElementById('pengembalianFields');

    statusDropdown.addEventListener('change', function () {
        const selectedStatus = this.value;

        if (selectedStatus === 'dikembalikan') {
            // If "Dikembalikan" is selected, show the nested "pengembalian" form fields
            pengembalianFields.style.display = 'block';
        } else {
            // If "Dipinjam" or any other status is selected, hide the nested "pengembalian" form fields
            pengembalianFields.style.display = 'none';
        }
    });
</script>
@endsection