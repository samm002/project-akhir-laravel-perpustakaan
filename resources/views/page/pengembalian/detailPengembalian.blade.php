@extends('layouts.master')

@section('title')
Show Pengembalian by Id = {{$pengembalian->id}}
@endsection

@section('content')

@if ($pengembalian)
<div class="card mb-4 d-flex px-auto">
    <div class="card-body">
      <p class="card-text"><b>Tanggal Pengembalian : </b>{{$pengembalian->tanggal_pengembalian}}</p>
      <p class="card-text"><b>Denda : </b>{{$pengembalian->denda}}</p>
    </div>
    @auth
    <div class="card-footer d-flex">
      <a href="/pengembalian/{{$pengembalian->id}}/edit" class="btn btn-warning btn-sm mx-2">Edit</a>
      <button type="button" class="btn btn-danger btn-sm mx-2" data-toggle="modal" data-target="#deleteModal{{$pengembalian->id}}">
        Delete
      </button>
    </div>
    @endauth
  </div>
@else
    <h1>Buku belum dikembalikan</h1>
@endif


@endsection
