@extends('layouts.master')

@section('title')
Isi Data Pengembalian
@endsection

@section('content')
<form action="/pengembalian" method="POST">
    @csrf
    <div class="form-group">
      <label>Tanggal Pengembalian</label>
      <input type="text" name="tanggal_pengembalian" class="@error('tanggal_pengembalian') is-invalid @enderror form-control" placeholder="Masukan Tanggal Pengembalian">
    </div>

    @error('tanggal_pengembalian')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Denda</label>
      <input type="text" name="denda" class="@error('denda') is-invalid @enderror form-control" placeholder="Masukan Denda">
    </div>

    @error('denda')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection