@extends('layouts.master')

@section('title')
Edit Petugas by Id = {{$petugas->id}} Form
@endsection

@section('content')
<form action="/petugas/{{$petugas->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
    <label>Nama</label>
    <input type="text" value="{{$petugas->nama}}" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Masukan Nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Email</label>
    <input type="text" value="{{$petugas->email}}" name="email" class="@error('email') is-invalid @enderror form-control" placeholder="Masukan Email">
    </div>
    @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Password</label>
    <input type="text" value="{{$petugas->password}}" name="password" class="@error('password') is-invalid @enderror form-control" placeholder="Masukan Password">
    </div>
    @error('password')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Nomor Telepon</label>
    <input type="text" value="{{$petugas->nomor_telepon}}" name="nomor_telepon" class="@error('nomor_telepon') is-invalid @enderror form-control" placeholder="Masukan Nomor Telepon">
    </div>
    @error('nomor_telepon')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<form action="/petugas/{{$petugas->id}}" method="POST">
    @csrf
    @method('delete')
        <input type="submit" value="Delete" class="btn btn-danger">
</form>

@endsection
