@extends('layouts.master')

@section('title')
List Petugas
@endsection

@section('content')
<h1>List Petugas</h1>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Email</th>
        <th scope="col">Nomor Telepon</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($petugas as $key => $petugas_item)
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$petugas_item->user->name}}</td>
            <td>{{$petugas_item->user->email}}</td>
            <td>{{$petugas_item->nomor_telepon}}</td>
            <td>
                <form action="/petugas/{{$petugas_item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/petugas/{{$petugas_item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/petugas/{{$petugas_item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-sm btn-danger">
                </form>
            </td>
          </tr>
        @empty
            <tr>
                <td>Belum ada data petugas</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection