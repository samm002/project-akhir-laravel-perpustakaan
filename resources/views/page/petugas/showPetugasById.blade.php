@extends('layouts.master')

@section('title')
Show Petugas by Id = {{$petugas->id}}
@endsection

@section('content')

<div class="card mb-4 d-flex px-auto">
  <div class="card-body">
    <p class="card-text"> <b>Nama: </b>{{$petugas->user->name}}</p>
    <p class="card-text"><b>Email : </b>{{$petugas->user->email}}</p>
    <p class="card-text"><b>Nomor Telepon : </b>{{$petugas->nomor_telepon}}</p>
  </div>
</div>
<a href="/petugas" class="btn btn-secondary btn-sm my-3">kembali</a>
@endsection