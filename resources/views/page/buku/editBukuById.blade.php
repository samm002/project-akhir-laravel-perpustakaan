@extends('layouts.master')

@section('title')
Edit Buku by Id = {{$buku->id}} Form
@endsection

@section('content')
<form action="/buku/{{$buku->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    
    <div class="form-group">
        <label>Judul</label>
        <input type="text" value="{{$buku->judul}}" name="judul" class="@error('judul') is-invalid @enderror form-control" placeholder="Masukan Judul Buku">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Tahun Terbit</label>
    <input type="text" value="{{$buku->tahun_terbit}}" name="tahun_terbit" class="@error('tahun_terbit') is-invalid @enderror form-control" placeholder="Masukan Tahun Terbit Buku">
    </div>
    @error('tahun_terbit')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Gambar</label>
        <input type="file" name="gambar" class="@error('gambar') is-invalid @enderror form-control" placeholder="Masukan Gambar Buku">
    </div>
    @error('gambar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Sinopsis</label>
    <textarea type="text" name="sinopsis" class="@error('sinopsis') is-invalid @enderror form-control" cols="30" rows="10" placeholder="Masukan Sinopsis Buku">{{$buku->sinopsis}}</textarea>
    </div>
    @error('sinopsis')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Genre</label>
        <select class="form-control" name="genre_id">
        <option value="">Pilih Genre</option>
        @forelse ($genre as $item)
            @if ($item->id === $buku->genre_id)
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
        @empty
            <option value="">Tidak Ada Genre</option>
        @endforelse
    </select>
    </div>
    @error('genre_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Penulis</label>
        <select class="form-control" name="penulis_id">
        <option value="">Pilih Penulis</option>
        @forelse ($penulis as $item)
            @if ($item->id === $buku->penulis_id)
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
        @empty
            <option value="">Tidak Ada Penulis</option>
        @endforelse
    </select>
    </div>
    @error('penulis_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Penerbit</label>
        <select class="form-control" name="penerbit_id">
        <option value="">Pilih Penerbit</option>
        @forelse ($penerbit as $item)
            @if ($item->id === $buku->penerbit_id)
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
        @empty
            <option value="">Tidak Ada penerbit</option>
        @endforelse
    </select>
    </div>
    @error('penerbit_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<form action="/buku/{{$buku->id}}" method="POST">
    @csrf
    @method('delete')
        <input type="submit" value="Delete" class="btn btn-danger">
</form>

@endsection
