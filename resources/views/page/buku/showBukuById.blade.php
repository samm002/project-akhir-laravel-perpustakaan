@extends('layouts.master')

@section('title')
Show Buku by Id = {{$buku->id}}
@endsection

@section('content')
<div class="card mb-4 d-flex px-auto">
  <img src="{{ asset('gambar_buku/' . $buku->gambar) }}" class="card-img-top ml-3" alt="Gambar buku {{ $buku->judul }}" style="max-width: 400px; height: auto;">
  <div class="card-body">
    <h5 class="card-title">{{$buku->judul}} - ({{$buku->tahun_terbit}})</h5><br>
    <p class="card-text"> <b>Genre : </b>{{$buku->genre->nama}}</p>
    <p class="card-text"> <b>Penulis : </b>{{$buku->penulis->nama}}</p>
    <p class="card-text"> <b>Penerbit : </b>{{$buku->penerbit->nama}}</p>
    <p class="card-text"> <b>Sinopsis : </b>{{$buku->sinopsis}}</p>
  </div>
  @auth
  <div class="card-footer d-flex">
    <a href="/buku/{{$buku->id}}/edit" class="btn btn-warning btn-sm mx-2">Edit</a>
    <button type="button" class="btn btn-danger btn-sm mx-2" data-toggle="modal" data-target="#deleteModal{{$buku->id}}">
      Delete
    </button>
  </div>
  @endauth
</div>
@endsection