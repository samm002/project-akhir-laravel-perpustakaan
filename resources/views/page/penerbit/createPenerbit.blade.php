@extends('layouts.master')

@section('title')
Isi Data Penerbit
@endsection

@section('content')
<form action="/penerbit" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Masukan Nama Penerbit">
    </div>

    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Tahun Berdiri</label>
      <input type="text" name="tahun_berdiri" class="@error('tahun_berdiri') is-invalid @enderror form-control" placeholder="Masukan Tahun Berdirinya Penerbit">
    </div>

    @error('tahun_berdiri')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Deskripsi</label>
      <textarea type="text" name="deskripsi" class="@error('deskripsi') is-invalid @enderror form-control" cols="30" rows="10" placeholder="Masukan Deskripsi Penerbit"></textarea>
    </div>

    @error('deskripsi')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection