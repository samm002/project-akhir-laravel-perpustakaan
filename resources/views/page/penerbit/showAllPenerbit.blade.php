@extends('layouts.master')

@section('title')
List penerbit
@endsection

@section('content')
<a href="/penerbit/create" class="btn btn-primary btn-sm my-3">Tambah</a>
<br><br>
<h1>List penerbit</h1>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Tahun Berdiri</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($penerbit as $key => $penerbit_item)
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$penerbit_item->nama}}</td>
            <td>{{$penerbit_item->tahun_berdiri}}</td>
            <td>
                <form action="/penerbit/{{$penerbit_item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/penerbit/{{$penerbit_item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/penerbit/{{$penerbit_item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-sm btn-danger">
                </form>
            </td>
          </tr>
        @empty
            <tr>
                <td>Belum ada data penerbit</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection