@extends('layouts.master')

@section('title')
Show Penerbit by Id = {{$penerbit->id}}
@endsection

@section('content')

<div class="card mb-4 d-flex px-auto">
  <div class="card-body">
    <p class="card-text"><b>Nama : </b>{{$penerbit->nama}}</p>
    <p class="card-text"><b>Tahun Berdiri : </b>{{$penerbit->tahun_berdiri}}</p>
    <p class="card-text"><b>Deskripsi : </b>{{$penerbit->deskripsi}}</p>
  </div>
</div>
<a href="/penerbit" class="btn btn-secondary btn-sm my-3">kembali</a>
@endsection