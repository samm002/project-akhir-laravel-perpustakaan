@extends('layouts.master')

@section('title')
Edit penerbit by Id = {{$penerbit->id}} Form
@endsection

@section('content')
<form action="/penerbit/{{$penerbit->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
    <label>Nama</label>
    <input type="text" value="{{$penerbit->nama}}" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Masukan Nama Penerbit">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Tahun Berdiri</label>
    <input type="text" value="{{$penerbit->tahun_berdiri}}" name="tahun_berdiri" class="@error('tahun_berdiri') is-invalid @enderror form-control" placeholder="Masukan Tahun Berdiri">
    </div>
    @error('tahun_berdiri')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea type="text" name="deskripsi" class="@error('deskripsi') is-invalid @enderror form-control" cols="30" rows="10" placeholder="Masukan Deskripsi Penerbit">{{ $penerbit->deskripsi }}</textarea>
      </div>
  
      @error('deskripsi')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<form action="/penerbit/{{$penerbit->id}}" method="POST">
    @csrf
    @method('delete')
        <input type="submit" value="Delete" class="btn btn-danger">
</form>

@endsection
