@extends('layouts.master')

@section('title')
List Peminjam
@endsection

@section('content')
<a href="/peminjam/create" class="btn btn-primary btn-sm my-3">Tambah</a>
<br><br>
<h1>List Peminjam</h1>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Email</th>
        <th scope="col">Alamat</th>
        <th scope="col">Nomor Telepon</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($peminjam as $key => $peminjam_item)
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$peminjam_item->nama}}</td>
            <td>{{$peminjam_item->email}}</td>
            <td>{{$peminjam_item->alamat}}</td>
            <td>{{$peminjam_item->nomor_telepon}}</td>
            <td>
                <form action="/peminjam/{{$peminjam_item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/peminjam/{{$peminjam_item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/peminjam/{{$peminjam_item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-sm btn-danger">
                </form>
            </td>
          </tr>
        @empty
            <tr>
                <td>Belum ada data peminjam</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection