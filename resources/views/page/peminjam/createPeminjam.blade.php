@extends('layouts.master')

@section('title')
Isi Data Peminjam
@endsection

@section('content')
<form action="/peminjam" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Masukan Nama">
    </div>

    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Email</label>
      <input type="text" name="email" class="@error('email') is-invalid @enderror form-control" placeholder="Masukan email">
    </div>

    @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Alamat</label>
      <input type="text" name="alamat" class="@error('alamat') is-invalid @enderror form-control" placeholder="Masukan Alamat">
    </div>

    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Nomor Telepon</label>
      <input type="text" name="nomor_telepon" class="@error('nomor_telepon') is-invalid @enderror form-control" placeholder="Masukan nomor_telepon">
    </div>

    @error('nomor_telepon')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection