@extends('layouts.master')

@section('title')
Show peminjam by Id = {{$peminjam->id}}
@endsection

@section('content')

<div class="card mb-4 d-flex px-auto">
  <div class="card-body">
    <p class="card-text"><b>Nama : </b>{{$peminjam->nama}}</p>
    <p class="card-text"><b>Email : </b>{{$peminjam->email}}</p>
    <p class="card-text"><b>Alamat : </b>{{$peminjam->alamat}}</p>
    <p class="card-text"><b>Nomor Telepon : </b>{{$peminjam->nomor_telepon}}</p>
  </div>
</div>
<a href="/peminjam" class="btn btn-secondary btn-sm my-3">kembali</a>
@endsection