@extends('layouts.master')

@section('title')
Edit peminjam by Id = {{$peminjam->id}} Form
@endsection

@section('content')
<form action="/peminjam/{{$peminjam->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
    <label>Nama</label>
    <input type="text" value="{{$peminjam->nama}}" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Masukan Nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Email</label>
    <input type="text" value="{{$peminjam->email}}" name="email" class="@error('email') is-invalid @enderror form-control" placeholder="Masukan Email">
    </div>
    @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Alamat</label>
    <input type="text" value="{{$peminjam->alamat}}" name="alamat" class="@error('alamat') is-invalid @enderror form-control" placeholder="Masukan Alamat">
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Nomor Telepon</label>
    <input type="text" value="{{$peminjam->nomor_telepon}}" name="nomor_telepon" class="@error('nomor_telepon') is-invalid @enderror form-control" placeholder="Masukan Nomor Telepon">
    </div>
    @error('nomor_telepon')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<form action="/peminjam/{{$peminjam->id}}" method="POST">
    @csrf
    @method('delete')
        <input type="submit" value="Delete" class="btn btn-danger">
</form>

@endsection
