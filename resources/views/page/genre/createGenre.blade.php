@extends('layouts.master')

@section('title')
Create Genre Form
@endsection

@section('content')
<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama / Jenis Genre</label>
      <input type="text" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Masukan Nama Genre">
    </div>

    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection