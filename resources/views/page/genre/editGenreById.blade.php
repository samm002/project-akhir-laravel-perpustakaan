@extends('layouts.master')

@section('title')
Edit Genre by Id = {{$genre->id}} Form
@endsection

@section('content')
<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
    <label>Nama / Jenis Genre</label>
    <input type="text" value="{{$genre->nama}}" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Masukan nama / jenis genre">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('delete')
        <input type="submit" value="Delete" class="btn btn-danger">
</form>

@endsection
