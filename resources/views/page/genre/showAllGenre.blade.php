@extends('layouts.master')

@section('title')
Show All Genre
@endsection

@section('content')
<a href="/genre/create" class="btn btn-primary btn-sm my-3">Tambah</a>
<br><br>
<h1>List Genre</h1>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $genre_item)
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$genre_item->nama}}</td>
            <td>
                <form action="/genre/{{$genre_item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/genre/{{$genre_item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/genre/{{$genre_item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-sm btn-danger">
                </form>
            </td>
          </tr>
        @empty
            <tr>
                <td>No Genre Data Inserted</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection