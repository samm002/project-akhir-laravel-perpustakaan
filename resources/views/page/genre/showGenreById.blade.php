@extends('layouts.master')

@section('title')
Show genre by Id = {{$genre->id}}
@endsection

@section('content')

<div class="card mb-4 d-flex px-auto">
  <div class="card-body">
    <h5 class="card-title"><b>Genre : </b>{{$genre->nama}}</h5>
  </div>
</div>
<a href="/genre" class="btn btn-secondary btn-sm my-3">kembali</a>
@endsection