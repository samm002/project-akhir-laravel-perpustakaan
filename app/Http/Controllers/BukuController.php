<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Buku;
use App\Models\Genre;
use App\Models\Penulis;
use App\Models\Penerbit;
use Illuminate\Support\Facades\File;


class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $buku = Buku::all();
        $genre = Genre::all();
        return view('page.buku.showAllBuku', ['buku' => $buku, 'genre' => $genre]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $genre = Genre::all();
        $penulis = Penulis::all();
        $penerbit = Penerbit::all();
        return view('page.buku.createBuku', ['genre' => $genre, 'penulis' => $penulis, 'penerbit' => $penerbit]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'tahun_terbit' => 'required|numeric',
            'gambar' => 'required|mimes:png,jpg,jpeg|max:16384',
            'sinopsis' => 'required',
            'genre_id' => 'required',
            'penulis_id' => 'required',
            'penerbit_id' => 'required',
        ]);

        $extension = $request->gambar->extension();
        $fileName = $request->judul;
        $timeAdded = date('Y-m-d_H-i-s'); 

        $gambarBuku = $fileName .'_'. $timeAdded .'.' . $extension;
        $request->gambar->move(public_path('gambar_buku'), $gambarBuku);

        $buku = new buku;

        $buku->judul = $request->input('judul');
        $buku->tahun_terbit = $request->input('tahun_terbit');
        $buku->gambar = $gambarBuku;
        $buku->sinopsis = $request->input('sinopsis');
        $buku->genre_id = $request->input('genre_id');
        $buku->penulis_id = $request->input('penulis_id');
        $buku->penerbit_id = $request->input('penerbit_id');

        $buku->save();

        return redirect('/buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $buku = buku::find($id);

        return view('page.buku.showbukuById', ['buku' => $buku]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $buku = Buku::find($id);
        $genre = Genre::all();
        $penulis = Penulis::all();
        $penerbit = Penerbit::all();

        return view('page.buku.editBukuById', ['buku' => $buku, 'genre' => $genre, 'penulis' => $penulis, 'penerbit' => $penerbit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'tahun_terbit' => 'required|numeric',
            'gambar' => 'mimes:png,jpg,jpeg|max:16384',
            'sinopsis' => 'required',
            'genre_id' => 'required',
            'penulis_id' => 'required',
            'penerbit_id' => 'required',
        ]);

        $buku = Buku::find($id);

        if($request->has('gambar')) {
            $path = "gambar_buku/";
            File::delete($path . $buku->gambar);
            
            $extension = $request->gambar->extension();
            $fileName = $request->judul;
            $timeAdded = date('Y-m-d_H-i-s'); 

            $gambarBuku = $fileName .'_'. $timeAdded .'.' . $extension;
            $request->gambar->move(public_path('gambar_buku'), $gambarBuku);
            $buku->gambar = $gambarBuku;
        }

        $buku->judul = $request->input('judul');
        $buku->tahun_terbit = $request->input('tahun_terbit');
        $buku->sinopsis = $request->input('sinopsis');
        $buku->genre_id = $request->input('genre_id');
        $buku->penulis_id = $request->input('penulis_id');
        $buku->penerbit_id = $request->input('penerbit_id');

        $buku->save();

        return redirect('/buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $buku = Buku::find($id);
        $path = "gambar_buku/";
        File::delete($path . $buku->gambar);

        $buku->delete();

        return redirect('/buku');
    }
}
