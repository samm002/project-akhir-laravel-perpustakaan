<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Transaksi;
use App\Models\Buku;
use App\Models\Peminjam;
use App\Models\Petugas;
use App\Models\Pengembalian;


class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $transaksi = Transaksi::all();
        $peminjam = Peminjam::all();
        $buku = Buku::all();
        $petugas = Petugas::all();
        $pengembalian = Pengembalian::all();
        return view('page.transaksi.showAllTransaksi', ['transaksi' => $transaksi, 'buku' => $buku, 'peminjam' => $peminjam, 'petugas' => $petugas, 'pengembalian' => $pengembalian]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $buku = Buku::all();
        $peminjam = Peminjam::all();
        $petugas = Petugas::all();
        $pengembalian = Pengembalian::all();
        return view('page.transaksi.createTransaksi', ['buku' => $buku, 'peminjam' => $peminjam, 'petugas' => $petugas, 'pengembalian' => $pengembalian]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'tanggal_peminjaman' => 'required',
            'status' => ['required', Rule::in(['dipinjam', 'dikembalikan'])],
            'buku_id' => 'required',
            'peminjam_id' => 'required',
            'petugas_id' => 'required',
            'pengembalian_id' => 'nullable',
        ]);

        $transaksi = new Transaksi;

        $transaksi->tanggal_peminjaman = $request->input('tanggal_peminjaman');
        $transaksi->status = $request->input('status');
        $transaksi->buku_id = $request->input('buku_id');
        $transaksi->peminjam_id = $request->input('peminjam_id');
        $transaksi->petugas_id = $request->input('petugas_id');
        $transaksi->pengembalian_id = $request->input('pengembalian_id');

        $transaksi->save();

        if ($request->input('status') === 'dikembalikan') {
            $pengembalian = new Pengembalian;
            $pengembalian->tanggal_pengembalian = $request->input('tanggal_pengembalian');
            $pengembalian->denda = $request->input('denda');
            $pengembalian->save();
            
            // Associate the pengembalian record with the transaksi
            $transaksi->pengembalian_id = $pengembalian->id;
            $transaksi->save();
        }

        return redirect('/transaksi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $transaksi = Transaksi::find($id);

        return view('page.transaksi.showTransaksiById', ['transaksi' => $transaksi]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $transaksi = Transaksi::find($id);
        $buku = Buku::all();
        $peminjam = Peminjam::all();
        $petugas = Petugas::all();
        $pengembalian = Pengembalian::all();

        return view('page.transaksi.edittransaksiById', ['transaksi' => $transaksi, 'buku' => $buku, 'penulis' => $peminjam, 'petugas' => $petugas, 'pengembalian' => $pengembalian]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tanggal_peminjaman' => 'required',
            'status' => ['required', Rule::in(['dipinjam', 'dikembalikan'])],
            'buku_id' => 'required',
            'peminjam_id' => 'required',
            'petugas_id' => 'required',
            'pengembalian_id' => 'nullable',
        ]);

        $transaksi = Transaksi::find($id);

        $transaksi->tanggal_peminjaman = $request->input('tanggal_peminjaman');
        $transaksi->status = $request->input('status');
        $transaksi->buku_id = $request->input('buku_id');
        $transaksi->peminjam_id = $request->input('peminjam_id');
        $transaksi->petugas_id = $request->input('petugas_id');
        $transaksi->pengembalian_id = $request->input('pengembalian_id');

        $transaksi->save();

        return redirect('/transaksi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $transaksi = Transaksi::find($id);

        $transaksi->delete();

        return redirect('/transaksi');
    }
}
