<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pengembalian;


class PengembalianController extends Controller
{

    public function create() {
        return view('page.pengembalian.createPengembalian');
    }
    
    public function store(Request $request) {
        $request->validate([
            'tanggal_pengembalian' => 'nullable|date',
            'denda' => 'nullable|numeric',
        ]);
        
        $pengembalian = new Pengembalian;
        $pengembalian->tanggal_pengembalian = $request->input('tanggal_pengembalian');
        $pengembalian->denda = $request->input('denda');
        $pengembalian->save();
        
        return redirect('/transaksi');
    }
    
    public function show($id) {
        $pengembalian = Pengembalian::find($id);
        return view('page.pengembalian.detailPengembalian', ['pengembalian' => $pengembalian]);
    }
    
    public function update(Request $request, $id) {
        $request->validate([
            'tanggal_pengembalian' => 'nullable|date',
            'denda' => 'nullable|numeric',
        ]);
        
        $pengembalian = Pengembalian::find($id);
        $pengembalian->tanggal_pengembalian = $request->input('tanggal_pengembalian');
        $pengembalian->denda = $request->input('denda');
        $pengembalian->save();

        return redirect('/transaksi');
    }

}
