<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Peminjam;

class PeminjamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $peminjam = Peminjam::all();
        return view('page.peminjam.showAllpeminjam', ['peminjam' => $peminjam]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $peminjam = Peminjam::all();
        return view('page.peminjam.createpeminjam', ['peminjam' => $peminjam]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'nomor_telepon' => 'required',
        ]);

        $peminjam = new Peminjam;
        $peminjam->nama = $request->input('nama');
        $peminjam->email = $request->input('email');
        $peminjam->alamat = $request->input('alamat');
        $peminjam->nomor_telepon = $request->input('nomor_telepon');
        $peminjam->save();

        return redirect('/peminjam');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $peminjam = Peminjam::find($id);

        return view('page.peminjam.showPeminjamById', ['peminjam' => $peminjam]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $peminjam = Peminjam::find($id);

        return view('page.peminjam.editPeminjamById', ['peminjam' => $peminjam]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'nomor_telepon' => 'required',
        ]);

        $peminjam = Peminjam::find($id);
        $peminjam->nama = $request->input('nama');
        $peminjam->email = $request->input('email');
        $peminjam->alamat = $request->input('alamat');
        $peminjam->nomor_telepon = $request->input('nomor_telepon');
        $peminjam->save();

        return redirect('/peminjam');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $peminjam = Peminjam::find($id);
        $peminjam->delete();

        return redirect('/peminjam');
    }
}
