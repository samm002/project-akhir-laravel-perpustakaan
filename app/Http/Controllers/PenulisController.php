<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penulis;

class PenulisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $penulis = Penulis::all();
        return view('page.penulis.showAllPenulis', ['penulis' => $penulis]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $penulis = Penulis::all();
        return view('page.penulis.createPenulis', ['penulis' => $penulis]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'usia' => 'required',
            'bio' => 'required',
        ]);

        $penulis = new Penulis;
        $penulis->nama = $request->input('nama');
        $penulis->usia = $request->input('usia');
        $penulis->bio = $request->input('bio');
        $penulis->save();

        return redirect('/penulis');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $penulis = Penulis::find($id);

        return view('page.penulis.showPenulisById', ['penulis' => $penulis]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $penulis = Penulis::find($id);

        return view('page.penulis.editPenulisById', ['penulis' => $penulis]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'usia' => 'required',
            'bio' => 'required',
        ]);

        $penulis = penulis::find($id);
        $penulis->nama = $request->input('nama');
        $penulis->usia = $request->input('usia');
        $penulis->bio = $request->input('bio');
        $penulis->save();

        return redirect('/penulis');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $penulis = Penulis::find($id);
        $penulis->delete();

        return redirect('/penulis');
    }
}
