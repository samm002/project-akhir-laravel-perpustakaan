<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penerbit;

class PenerbitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $penerbit = Penerbit::all();
        return view('page.penerbit.showAllPenerbit', ['penerbit' => $penerbit]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $penerbit = Penerbit::all();
        return view('page.penerbit.createPenerbit', ['penerbit' => $penerbit]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'tahun_berdiri' => 'required',
            'deskripsi' => 'required',
        ]);

        $penerbit = new Penerbit;
        $penerbit->nama = $request->input('nama');
        $penerbit->tahun_berdiri = $request->input('tahun_berdiri');
        $penerbit->deskripsi = $request->input('deskripsi');
        $penerbit->save();

        return redirect('/penerbit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $penerbit = Penerbit::find($id);

        return view('page.penerbit.showPenerbitById', ['penerbit' => $penerbit]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $penerbit = Penerbit::find($id);

        return view('page.penerbit.editPenerbitById', ['penerbit' => $penerbit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'tahun_berdiri' => 'required',
            'deskripsi' => 'required',
        ]);

        $penerbit = penerbit::find($id);
        $penerbit->nama = $request->input('nama');
        $penerbit->tahun_berdiri = $request->input('tahun_berdiri');
        $penerbit->deskripsi = $request->input('deskripsi');
        $penerbit->save();

        return redirect('/penerbit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $penerbit = Penerbit::find($id);
        $penerbit->delete();

        return redirect('/penerbit');
    }
}
