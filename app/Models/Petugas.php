<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Petugas extends Model
{
    use HasFactory;

    protected $table = 'petugas';

    protected $fillable = ['nomor_telepon', 'user_id'];

    public function user() 
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function transaksi() {
        return $this->hasMany(Transaksi::class, 'petugas_id');
    }
}
