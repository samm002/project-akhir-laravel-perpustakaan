<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;

    protected $table = 'transaksi';

    protected $fillable = ['tanggal_peminjam', 'peminjam_id', 'buku_id', 'petugas_id', 'pengembalian_id'];

    public function buku()
    {
        return $this->belongsTo(buku::class, 'buku_id');
    }

    public function pengembalian()
    {
        return $this->belongsTo(Pengembalian::class, 'pengembalian_id');
    }
    
    public function petugas()
    {
        return $this->belongsTo(Petugas::class, 'petugas_id');
    }
    
    public function peminjam()
    {
        return $this->belongsTo(Peminjam::class, 'peminjam_id');
    }
    public function setPengembalianIdAttribute($value)
    {
        if ($this->attributes['status'] == 'dipinjam') {
            $this->attributes['pengembalian_id'] = null;
        } else {
            $this->attributes['pengembalian_id'] = $value;
        }
    }

}
