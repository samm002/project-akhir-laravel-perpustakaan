<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Peminjam extends Model
{
    use HasFactory;

    protected $table = 'peminjam';

    protected $fillable = ['nama', 'email', 'nomor_telepon', 'alamat'];

    public function transaksi()
    {
        return $this->hasMany(Transaksi::class, 'peminjam_id');
    }
}
