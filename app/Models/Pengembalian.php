<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengembalian extends Model
{
    use HasFactory;

    protected $table = 'pengembalian';

    protected $fillable = ['tanggal_pengembalian', 'denda'];

    public function transaksi() 
    {
        return $this->hasMany(Transaksi::class, 'pengembalian_id');
    }
}
