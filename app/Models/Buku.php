<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    use HasFactory;

    protected $table = 'buku';

    protected $fillable = ['judul', 'tahun_terbit', 'gambar', 'sinopsis', 'genre_id', 'penulis_id', 'penerbit_id'];

    public function genre() 
    {
        return $this->belongsTo(Genre::class, 'genre_id');
    }

    public function penulis() 
    {
        return $this->belongsTo(Penulis::class, 'penulis_id');
    }

    public function penerbit() 
    {
        return $this->belongsTo(Penerbit::class, 'penerbit_id');
    }

}
