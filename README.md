## Final Project Perpustakaan Online

## Kelompok 2
- Muhammad Naufal Ammar Rizqi (@Mnaufalar15)
- Samuel Djodi (@samuel_djodi)
- Adi hardiansyah (@adihardiansyah)

## Tema Project
Perpustakaan

## ERD DIAGRAM
[ERD](https://drive.google.com/file/d/1nazY7TFl3sRbwYvKNZTkpocl9Dhi_zI8/view?usp=sharing)
<br>
<img src="https://drive.google.com/file/d/1nazY7TFl3sRbwYvKNZTkpocl9Dhi_zI8/view?usp=sharing" alt="Alt text" title="ERD Diagram">

## Link Video
- Video Demo : https://drive.google.com/file/d/1D7_XRoqxBppvpgKcSQZ5k8OngK4NF_Cs/view?usp=sharing
- Link Deploy : (Tidak di deploy)
