<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksi', function (Blueprint $table) {
            // Ubah definisi kolom status
            DB::statement('ALTER TABLE transaksi MODIFY status ENUM("dipinjam", "dikembalikan") DEFAULT "dipinjam"');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi', function (Blueprint $table) {
            // Rollback ke definisi kolom status sebelumnya jika perlu
            DB::statement('ALTER TABLE transaksi MODIFY status ENUM("dipinjam", "tersedia") DEFAULT "tersedia"');
        });
    }
};
