<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\PeminjamController;
use App\Http\Controllers\PetugasController;
use App\Http\Controllers\PenerbitController;
use App\Http\Controllers\PenulisController;
use App\Http\Controllers\PengembalianController;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\TransaksiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PetugasController::class, 'index']);
Route::resource('genre', GenreController::class);
Route::resource('peminjam', PeminjamController::class);
Route::resource('petugas', PetugasController::class);
Route::resource('penerbit', PenerbitController::class);
Route::resource('penulis', PenulisController::class);
Route::get('/pengembalian/create', [PengembalianController::class, 'create']);
Route::get('/pengembalian/{id}', [PengembalianController::class, 'show'])->name('pengembalian.show');
Route::post('/pengembalian', [PengembalianController::class, 'store']);
Route::put('/pengembalian/{id}', [PengembalianController::class, 'update']);
Route::resource('buku', BukuController::class);
Route::resource('transaksi', TransaksiController::class);

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
